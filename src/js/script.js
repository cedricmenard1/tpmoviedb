
var intervalId = 0;

function scrollStep(scrollStepHeight) {
    if(window.pageYOffset === 0) {
        clearInterval(intervalId);
    }
    window.scrollTo(0, window.pageYOffset - scrollStepHeight);
}

function scrollToTop(scrollStepHeight, delay) {
    if(scrollStepHeight <= 0) {
        alert("The specified scroll step height must be positive!");
    } else if(delay <= 0) {
        alert("The specified scroll delay must be positive!");
    }
    intervalId = setInterval(function() {
        scrollStep(scrollStepHeight);
    }, delay);

}


document.addEventListener("DOMContentLoaded", function(){


    var connexion = new MovieDb();

    connexion.requetePopular();
    connexion.requeteLatest();

    retour.addEventListener("click", function() {
        scrollToTop(50, 5.6);});

    var swiper = new Swiper('.swiper-container', {
        pagination: {
            el: '.swiper-pagination',
            type: 'progressbar',
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
    setTimeout(() => {
        swiper.update()
    }, 100);
});




class MovieDb{

    constructor(){

        this.APIkey = "18d06fed0d405aa9b740edb74ad7867f";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "http://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 6;

        this.totalSwip = 9;

        this.totalActeur = 6;

        this.idMovie = "fiche-film.html?id=";

    }


    requetePopular(){

        var xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.retourRequetePopular.bind(this));

        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();
    }


    retourRequetePopular(e){

        let target = e.currentTarget;

        let data;

        if(target.readyState === target.DONE){

            data = JSON.parse(target.responseText).results;

            console.log(data);

            this.affichePopular(data)
        }

    }


    affichePopular(data){

        for (let i = 0; i < this.totalFilm; i++){
            console.log(data[i]);


            var unArticle = document.querySelector(".template>article.film").cloneNode(true);

            unArticle.querySelector("h2").innerHTML = data[i].title;

            unArticle.querySelector("a").setAttribute("href", this.idMovie + data[i].id);

            if (data[i].overview == ""){

                unArticle.querySelector(".description").innerHTML = "Aucune description pour ce film 😞";

            }

            else{
                unArticle.querySelector(".description").innerHTML = data[i].overview;
            }

            var uneImage = unArticle.querySelector("img");

            uneImage.setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);

            document.querySelector(".liste-films").appendChild(unArticle);
        }





    }


    requeteLatest(){

        var xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.retourRequeteLatest.bind(this));

        xhr.open("GET", this.baseURL + "movie/now_playing?page=1&language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();
    }


    retourRequeteLatest(e){

        let target = e.currentTarget;

        let data;

        if(target.readyState === target.DONE){

            data = JSON.parse(target.responseText).results;

            console.log(data);

            this.afficheLatest(data)
        }

    }


    afficheLatest(data){

        for (let i = 0; i < this.totalSwip; i++){

            var unSwiper = document.querySelector(".template>.swiper-slide").cloneNode(true);

            unSwiper.querySelector("h2").innerHTML = data[i].title;

            unSwiper.querySelector("h3").innerHTML = data[i].vote_average;

            unSwiper.querySelector("a").setAttribute("href", this.idMovie + data[i].id);

            var uneImage = unSwiper.querySelector("img");

            uneImage.setAttribute("src", this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);

            document.querySelector(".swiper-wrapper").appendChild(unSwiper);
        }



    }

}